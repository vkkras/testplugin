<?php
/*
Plugin Name:       Test plugin
Description:       Plugin will provide a public REST API endpoint my-json/v1/localize that will output following data on GET request.
Version:           1.0
Requires at least: 5.7
Requires PHP:      8.0
Author:            Victor K.
*/

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

add_action( 'rest_api_init', function () {
    register_rest_route( 'my-json/v1', '/localize', array(
        'methods' => 'GET',
        'callback' => 'localize_func',
    ) );
} );

function localize_func() {
    $apiURL = 'https://freegeoip.app/json/'.$_SERVER['REMOTE_ADDR'];
    
    $ch = curl_init($apiURL);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $apiResponse = curl_exec($ch);
    curl_close($ch);
    $ipData = json_decode($apiResponse, true);
    
    if (!empty($ipData)) {
        $location = $ipData['latitude'].', '.$ipData['longitude'];
    }
        else $location = 'Not defined';
    
    $response = array(
        'current_time' => date('d-m-Y h:i:s'),
        'remote_ip' => $_SERVER['REMOTE_ADDR'],
        'location' => $location
    );
    return $response;
}