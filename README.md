# README #

Test plugin is Wordpress plugin that provide a public REST API endpoint my-json/v1/localize that output location data on GET request.

### How to install ###

* Create folder wp-content/plugins/test-plugin/
* Copy plugin file test-pugin.php to your Wordpress site plugins folder wp-content/plugins/test-plugin/
* Open Plugins section in admin area of your website
* Do plugin activation

### How to use ###

* Open your website by URL yoursite.com/wp-json/my-json/v1/localize
* You'll get result in json format for your needs